from nose.util import *
from nose.tools import *
import acp_times
import arrow

#time object of now for be reused in acp_times tests
now = arrow.now()

def test_open_time_175():
    assert( now.shift(hours=5,minutes=9).isoformat() == acp_times.open_time(175,200,now.isoformat()))

def test_open_time_150():
    assert( now.shift(hours=4,minutes=25).isoformat() == acp_times.open_time(150,300,now.isoformat()))
    
def test_open_time_120():
    assert( now.shift(hours=3,minutes=32).isoformat() == acp_times.open_time(120,300,now.isoformat()))

def test_open_time_60():
    assert( now.shift(hours=1,minutes=46).isoformat() == acp_times.open_time(60,200,now.isoformat()))

def test_close_time_175():
    assert( now.shift(hours=11,minutes=40).isoformat() == acp_times.close_time(175,200,now.isoformat()))


def test_close_time_0():
    assert(now.shift(hours=1).isoformat() == acp_times.close_time(0,200,now.isoformat()) )

#rusa.org by rule this should 13H30 shift by rule, where as it would 13H30 by the default math
def test_close_200_edgecase():
    assert(now.shift(hours=13,minutes=30).isoformat() == acp_times.close_time(200,200,now.isoformat()) )

@raises(Exception)
def test_open_time_overdist():
    acp_times.open_time(250, 200, now)

@raises(Exception)
def test_close_time_overdist():
    acp_times.close_time(250, 200, now)

@raises(Exception)
def test_close_time_neg_brevet():
    assert( now.shift(hours=4,minutes=25).isoformat() == acp_times.close_time(150,-300,now.isoformat()))

@raises(Exception)
def test_close_time_neg_control():
    assert( now.shift(hours=4,minutes=25).isoformat() == acp_times.close_time(-150,300,now.isoformat()))

@raises(Exception)
def test_open_time_neg_brevet():
    assert( now.shift(hours=4,minutes=25).isoformat() == acp_times.open_time(150,-300,now.isoformat()))

@raises(Exception)
def test_open_time_neg_control():
    assert( now.shift(hours=4,minutes=25).isoformat() == acp_times.open_time(-150,300,now.isoformat()))

#test for helper methods

@raises(Exception)
def test_control_normialize():
    acp_times.control_normialize(200,250)

def test_break_control_900():
    test_dict = acp_times.break_control_dist(900)
    assert(300 == test_dict['600-1000'])

def test_break_control_0():
    test_dict = acp_times.break_control_dist(0)
    assert(0 == test_dict['600-1000'])
    assert(0 == test_dict['400-600'])
    assert(0 == test_dict['200-400'])
    assert(0 == test_dict['0-200'])